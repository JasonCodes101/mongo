const DudeHeaders = require('../models/Dude_profile_schema');

var Mongoose = require('mongoose');
var ObjectId = Mongoose.Types.ObjectId;

exports.addDudeDetails = function (req, res, next) {
  console.log('req.body.DudeHeaders ',req.body.DudeHeaders);
    try {
        DudeHeaders.insertMany(req.body.DudeHeaders, function (err, result) {
            if (err) {
                return res.send({
                    success: false,
                    message: 'Error accessing database.',
                    data: err,
                    status: 422
                });
            } else if (!result) {
                return res.send({
                    success: false,
                    message: 'Error occured while inserting profile details',
                    data: result,
                    status: 400
                });
            } else {
                return res.send({
                    success: true,
                    message: 'Profile details inserted successfully',
                    data: result,
                    status: 200
                });
            }
        });
    } catch (error) {
        console.log(error);
        return res.send({
            success: false,
            message: 'Processing error. Please check input data and try again',
            data: error,
            status: 205
        });
    }
}

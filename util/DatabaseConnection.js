const mongoose = require('mongoose');

exports.openDBConnection = function () {
    mongoose.Promise = global.Promise;
    mongoose.connect('mongodb://localhost:27017/JASON', { useMongoClient: true }).
        then((response) => {
            console.log('successfully connected to database');
        }).catch((err) => {
            console.log('connection error: ', err);
        });
}

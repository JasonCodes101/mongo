const express = require('express');
const mongoose = require('mongoose');
const bodyParser = require('body-parser');

const DatabaseConnection = require('./util/DatabaseConnection');
const Routes = require('./routes/index');
const Config = require('./config/index');

//Express
const app = express();
app.use(bodyParser.urlencoded({extended: true}));
app.use(bodyParser.json());

app.use(function (req, res, next) {
    res.header("Access-Control-Allow-Origin", "*");
    res.header("Access-Control-Allow-Headers", "Origin, X-Requested-With, Content-Type, Accept");
    next();
});

//Catach if uncaught exception throws in the middle of server running 
process.on('uncaughtException', function (error) {
    console.log(error.stack);
});

//Routes
app.use('/api', Routes);

//Start server
app.listen(Config.API_PORT, function () {
    console.log('Benji API is running on port ', Config.API_PORT);
});

module.exports = app
const Express = require('express');

const Profile = require('./DudeHeaders');

const DatabaseConnection = require('../util/DatabaseConnection');
DatabaseConnection.openDBConnection();

const Router = Express.Router();
Router.use('/DudeHeaders', Profile);

// export router
module.exports = Router;

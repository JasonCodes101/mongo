const mongoose = require('mongoose');
const Schema = mongoose.Schema;

const DudeSchema = new Schema ({
    NAME: { type: String, trim: true },
    GENDER: { type: String, trim: true },
    HOBBY: { type: String, trim: true },
    GRADE: { type: Number, trim: true }
});

const DudeProfile = mongoose.model('DudeProfile', DudeSchema);
module.exports = DudeProfile;
